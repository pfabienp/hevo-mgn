
//$fn = $preview ? 0 : 128;

z3in1 = 1;

M5hole = 5.5;
M5head = 11.5;

M3insertD = 4.7;
M3insertH = 5.5;
M3insertD_small = 3.6;  // https://www.aliexpress.com/item/32810928309.html
M3insertD_bltouch = M3insertD_small;
M3insertD_Xsensor = M3insertD_small;
M3hole = 3.4;
M3head = 6.2;
M3thread = 2.5; // screw directly to plastics

xDiff = 22;
xDiff2 = 49-xDiff;

LM12_D = 21;

motorScrewPos = [[31/2,31/2,0],[-31/2,31/2,0],[31/2,-31/2,0],[-31/2,-31/2,0]];

motorPlus = 7;

zPos = 30; /* >=25 ~ <(zLen - 8) */
zLen = 80;

module tableCorner(y=-80, z=zPos)
{
  %color("gray",0.6) translate([5.25,y-15,z-10]) 
    rotate([0,90,90])
      import ("Z-Axis_LM12LUU_Bearing_Holder.stl"); 
}

//  *%color("gray",0.6) translate([-18.5,-77.5,-35]) 
//    rotate([0,0,180])
//      import("Z_Axis_Linear_Rail_Bracket_-_Left_1.0.stl"); //  https://www.thingiverse.com/thing:2254103/files


module nema17_T8(len=40, shaft=zLen+10 )
{
  difference()
  {
    intersection()
    {
      translate([-42.3/2,-42.3/2,-len+0.01]) cube ([42.3,42.3,len-0.02]);
      
      rotate([0,0,45]) translate([-52/2,-52/2,-len]) cube ([52,52,len]);
    }
    translate([0,0,-9]) 
      for (p = motorScrewPos)
        translate(p) cylinder(d=3,h=10);
  }
  cylinder(d=22,h=2);
  //cylinder(d=5,h=24);
  cylinder(d=8, h=shaft);
  
}

module bottomA(yPlus)
{
  hull()
  {
    translate([-25-xDiff,-(43+2*5)/2,-30+5+2]) 
      cube ([25+43/2+xDiff,43+2*5,25-2-(motorPlus-5)]);
    translate([-25-xDiff,-(43+2*5)/2+0.8*motorPlus,-30+5+2+5]) 
      cube ([25+43/2+xDiff-0.8*motorPlus,43+2*5-1.6*motorPlus,25-2]);
    cylinder(d2=30+2,h=5);
  }
  translate([-30-xDiff+5,-(43+10+2*yPlus)/2,-30+5+10]) cube([30,43+10+2*yPlus,30-10]);
  hull()
  {
    translate([-xDiff+5,(43+10+2*yPlus)/2-15/2,-30/2]) rotate([0,90,0]) cylinder(d1=20, d2=15, h=5);
    translate([-xDiff,(43+10+2*yPlus)/2-15/2,-30/2]) rotate([0,90,0]) cylinder(d1=20, d2=15, h=5);
    translate([-xDiff+5,-(43+10+2*yPlus)/2+15/2,-30/2]) rotate([0,90,0]) cylinder(d1=20, d2=15, h=5);
    translate([-xDiff,-(43+10+2*yPlus)/2+15/2,-30/2]) rotate([0,90,0]) cylinder(d1=20, d2=15, h=5);
  }
}
module bottomR(yPlus)
{
  translate([-43/2+5,-(43+10+2*yPlus)/2,-30+5])
    rotate([0,-21,0])
      translate([0,0,-30])
        cube([60,43+10+2*yPlus,30]);  
  translate([-43/2-3,-43/2,-motorPlus-50]) cube([43+5,43,50]);
  translate([-30-xDiff,-100/2,-30]) cube([30,100,30]);
  translate([-43/2-30/2,(43+10+2*yPlus)/2-15,0]) cylinder(d=M5hole, h=40);
  translate([-43/2-30/2,-(43+10+2*yPlus)/2+15,0]) cylinder(d=M5hole, h=40);
  translate([-43/2-30/2,(43+10+2*yPlus)/2-15/2,-30/2]) rotate([0,90,0]) cylinder(d=M5hole, h=40);
  translate([-43/2-30/2,-(43+10+2*yPlus)/2+15/2,-30/2]) rotate([0,90,0]) cylinder(d=M5hole, h=40);
  translate([-xDiff+5,(43+10+2*yPlus)/2-15/2,-30/2]) rotate([0,90,0]) cylinder(d=11, h=40);
  translate([-xDiff+5,-(43+10+2*yPlus)/2+15/2,-30/2]) rotate([0,90,0]) cylinder(d=11, h=40);

  for (p = motorScrewPos)
  {
    translate([0,0,5-motorPlus-0.2]) translate(p) rotate([180,0,0]) cylinder(d=M3hole, h=20);
    translate([0,0,5-motorPlus]) translate(p) cylinder(d=M3head, h=20);
  }

  translate([0,0,-20]) cylinder(d=22,h=40);
  translate([0,0,-motorPlus+2]) cylinder(d1=22,d2=30,h=8);
  translate([0,0,-motorPlus+2+8]) cylinder(d=30,h=40);
}
module bottom()
{
  yPlus = 13;
  difference()
  {
    bottomA(yPlus);
    bottomR(yPlus);
  }
}

module middleA(z3in1)
{
  hull()
  {
    translate([-15,(5+30+5)/-2,0]) cube([18+xDiff2+15,5+30+5,5]);
    translate([xDiff2-5,(5+30+5)/-2,-18]) cube([18+5,5+30+5,18+5]);
  }
  translate([xDiff2-5,(19+5+30+5+19)/-2,-10]) cube([18+5,19+5+30+5+19,10+5]);
  if (!z3in1) hull()
  {
    translate([xDiff2-10,+((5+30+5)/2+10),-10]) rotate([0,90,0]) cylinder (d1=14,d2=18,h=5);
    translate([xDiff2,+((5+30+5)/2+10),-10]) rotate([0,90,0]) cylinder (d1=14,d2=18,h=5);
    translate([xDiff2-10,-((5+30+5)/2+10),-10]) rotate([0,90,0]) cylinder (d1=14,d2=18,h=5);
    translate([xDiff2,-((5+30+5)/2+10),-10]) rotate([0,90,0]) cylinder (d1=14,d2=18,h=5);
  }
}

module middleR(z3in1)
{
  translate([xDiff2,(20+5+30+5+20)/-2-1,0])
    rotate([0,90,0])
      cube([19+5,1+20+5+30+5+20+1,19+5]);
  translate([-15,(30)/-2,-20]) cube([xDiff2+15-5.01,30,20]);
  translate([0,0,-20]) cylinder (d=9.5,h=40);
  translate([0,0,-5+1.5]) cylinder (d=11,h=5);

  // T8 nut
  rotate ([0,0,45+0*90]) translate([0, 8,-20]) cylinder (d=M3hole,h=40);
  rotate ([0,0,45+1*90]) translate([0, 8,-20]) cylinder (d=M3hole,h=40);
  rotate ([0,0,45+2*90]) translate([0, 8,-20]) cylinder (d=M3hole,h=40);
  rotate ([0,0,45+3*90]) translate([0, 8,-20]) cylinder (d=M3hole,h=40);

  if (z3in1)
  {
    translate([xDiff2+10,0,-20]) cylinder (d=M5hole,h=40);
  }
  else
  {
    translate([xDiff2+10,+((5+30+5)/2+5),-20]) cylinder (d=M5hole,h=40);
    translate([xDiff2+10,-((5+30+5)/2+5),-20]) cylinder (d=M5hole,h=40);
  }
  if (z3in1)
  {
    translate([xDiff2-11,+((30+2)/2+10),-10]) rotate([0,90,0]) cylinder (d=M5hole,h=40);
    translate([xDiff2-11,-((30+2)/2+10),-10]) rotate([0,90,0]) cylinder (d=M5hole,h=40);
    *translate([xDiff2-11,+((30+2)/2+10),-10]) rotate([0,90,0]) cylinder (d=M5head,h=6);
    *translate([xDiff2-11,-((30+2)/2+10),-10]) rotate([0,90,0]) cylinder (d=M5head,h=6);
  }
  else
  {
    translate([xDiff2-11,+((5+30+5)/2+10),-10]) rotate([0,90,0]) cylinder (d=M5hole,h=40);
    translate([xDiff2-11,-((5+30+5)/2+10),-10]) rotate([0,90,0]) cylinder (d=M5hole,h=40);
    translate([xDiff2-11,+((5+30+5)/2+10),-10]) rotate([0,90,0]) cylinder (d=M5head,h=6);
    translate([xDiff2-11,-((5+30+5)/2+10),-10]) rotate([0,90,0]) cylinder (d=M5head,h=6);
  }
}

module middle()
{
  difference()
  {
    middleA(z3in1 = false);
    middleR(z3in1 = false);
  }
}
module topA(zOff, z3in3, bearingPlus, topHole)
{
  translate([0,0,zOff-bearingPlus]) cylinder(d2=22+2*5, d1=22+2*5-2*2, h=2);
  translate([0,0,zOff+2-bearingPlus]) cylinder(d=22+2*5, h=10-2-2+bearingPlus);
  translate([0,0,zOff+10-2]) cylinder(d1=22+2*5, d2=22+2*5-2*2, h=2);
  hull()
  {
    translate([-0.01,-25/2,(zOff >= 0) ? zOff : 0])
      cube([0.01,25,(zOff >= 0) ? 10 : (10 + zOff)]);
    if (z3in3)
    {
      translate([-28-xDiff,-25/2,0])
        cube([28,25,5]);
    }
    else
    {
      translate([-28-xDiff,-25/2,-8])
        cube([28,25,5+8]);
    }
    translate([-15-xDiff,0,1]) 
      cylinder(d=M5head+4, h=10-1);
    delta = ((zOff <=5) ? (zOff + 5) : 10) * 0.8;
    y2 = 30-2*(delta);
    translate([-28-xDiff+delta,-y2/2,zOff+5])
      cube([28-delta,y2,5]);
  }
}
module topR(zOff, bearingPlus, topHole)
{
  translate([0,0,zOff + ((topHole) ? 10+1 : 10-2)]) rotate([180,0,0]) cylinder(d=22, h=30);
  translate([0,0,zOff + 10-1.5]) rotate([180,0,0]) cylinder(d=22-2, h=30);

  translate([-xDiff+5,-25,zOff-50-bearingPlus]) cube([50,50,50]);
  translate([-xDiff-50,-25,-50]) cube([50,50,50]);
  
  // M5 screws (H)
  translate([-15-xDiff,0,5-0.2])
    rotate([180,0,0])
      cylinder(d=M5hole, h=6);
  translate([-15-xDiff,0,5])
    cylinder(d=M5head, h=12+zOff);

}

module top(zOff = 2, bearingPlus=0, topHole = true)
{
  difference(){
    topA(zOff, false, bearingPlus, topHole);
    topR(zOff, bearingPlus, topHole);
  }
}

module top3in1(pitch = 80, zOff = 2, bearingPlus=0, topHole = true)
{
  yOff = 7+4;
  angle = 60;//55;
  
  barL = pitch - yOff;
  
  render () difference()
  {
    union()
    {
      translate([0,-pitch,0]) render() topRodA(zOff, yOff, angle);
      topA(zOff, true, bearingPlus, topHole);
      mirror([0,1,0]) translate([0,-pitch,0]) render() topRodA(zOff, yOff, angle);
      difference()
      {
        hull()
        {
          translate([-xDiff-22,-barL,-10]) cube([22+2,2*barL,2+10]);
          translate([-xDiff-22+(zOff+5),-barL,zOff+10-1]) cube([22+2-2*(zOff+5),2*barL,1]);
        }
        translate([-xDiff-50,-barL-1,-50]) cube([50,2*barL+2,50]);
        difference()
        {
          hull()
          {
            translate([-xDiff-22+2+0.5,-barL,-8]) cube([22-2-0.5,2*barL,2+8]);
            translate([-xDiff-22+(zOff+5)+2+0.5,-barL,zOff+10-1]) cube([22-2-2*(zOff+5)-0.5,2*barL,1]);
          }
          translate([-xDiff-22+(zOff+5),-barL,zOff+10-1.5]) cube([22+2-2*(zOff+5),2*barL,2]);
        }
        
        if ((zOff > 1) && (pitch >= 78)) translate ([-xDiff-22+0.2,-15,2])
        {
          rotate([atan2(zOff+10-2,zOff+5),0,-90])
            {
              *translate([-1+(pitch-80)/2,7,0])
                linear_extrude(height = 1)
                  text("thingiverse.com", size=4);
              *translate([(pitch-80)/2,1.5,0])
                linear_extrude(height = 1)
                  text("/thing:4663434", size=4);
              translate([-pitch+14+(pitch-80)/2,3,0])
                linear_extrude(height = 1)
                  text("Don’t Panic.", size=5);
            }
        }
      }
      translate([-xDiff+2-0.5,0,-10+2])
        rotate([90,0,90])
            linear_extrude(height = 0.5+0.25)
              text("thingiverse.com/thing:4663434", size=5, halign="center", font="Liberation Sans:style=Bold");

    }
    translate([0,-pitch,0]) render() topRodR(zOff, yOff, angle);
    topR(zOff, bearingPlus, topHole);
    mirror([0,1,0]) translate([0,-pitch,0]) render() topRodR(zOff, yOff, angle);
    *translate([xDiff2,-pitch-1,-18]) cube([5+18,2*pitch+2,18]);
  }
}

module topRodA(zOff, yOff, angle)
{
  translate([0,0,zOff]) hull()
  {
    cylinder(d=12+2*5,h=10);
    translate([-xDiff,-15+yOff,0])
     cube([5,30,10]);
  }
  translate([0-xDiff,-15+yOff,zOff])
    cube([5,30,10]);

  hull()
  {
    translate([0-xDiff-0.01,-15+yOff,(zOff >= 0) ? zOff : 0])
      cube([0.01,30,(zOff >= 0) ? 10 : (10 + zOff)]);
    translate([-28-xDiff,-15+yOff,0])
      cube([28,30,5]);
    translate([-15-xDiff,yOff,1]) 
      cylinder(d=M5head+4, h=10+zOff-1);
    delta = ((zOff <=5) ? (zOff + 5) : 10) * 0.8;
    y2 = 30-2*(delta);
    translate([-28-xDiff+delta,-y2/2+yOff,zOff+5])
      cube([28-delta,y2,5]);
  }
  hull()
  {
    translate([-xDiff,-15+yOff,-28])
      cube([2,30,30+zOff]);
    translate([-xDiff,-15+yOff+6,-28])
      cube([5,30-6,30+zOff]);
  }
  translate([5-xDiff,yOff+5,-15]) rotate([0,90,0]) cylinder(d1=M5head+7, d2=M5head+4, h=2.5);
}
module topRodR(zOff = 2, yOff, angle)
{
  translate([0,0,-10]) cylinder(d=12-0.01,h=30+zOff); // Z rod
  rotate([0,0,90+angle]) translate([0,-5,zOff]) 
    {
      translate([0,0,-1]) cube([16,1,12]);
      translate([16,-10+1,-1]) cube([1,10,12]);
      translate([0,0,-1]) cube([5,5,12]);
      translate([6+4,-20,5]) rotate([-90,0,0]) cylinder(d=M3hole,h=50);
      translate([6+4,10,5]) rotate([-90,0,0]) cylinder(d=M3insertD,h=20);
    }
    
  // M5 screws (H)
  translate([-15-xDiff,yOff,5-0.2])
    rotate([180,0,0])
      cylinder(d=M5hole, h=6);
  translate([-15-xDiff,yOff,5])
    cylinder(d=M5head, h=12+zOff);
  // M5 screws (V)
  translate([-1-xDiff,yOff+5,-15])
    rotate([0,90,0]) cylinder(d=M5hole, h=12);
  translate([5-xDiff,yOff+5,-15])
    rotate([0,90,0]) cylinder(d=M5head, h=12);
}

module topRod(zOff = 2)
{
  yOff = 7+4;
  angle = 60;//55;
  difference()
  {
    topRodA(zOff, yOff, angle);
    topRodR(zOff, yOff, angle);
  }
}

module frameCornerA(z3in1)
{
  hull()
  {
    translate([xDiff2,-12-3,-18]) cube([18,40,5+18]);
    translate([0,0,-18]) cylinder (d=21+2*5, h=5+18);
  }
  hull()
  {
    translate([18/-2,-12-18,-18]) cube([20,18,5+18]);
    translate([0,0,-18]) cylinder (d=21+2*5, h=5+18);
    translate([-xDiff+2.5,-12-14,-14]) cube([20,5+14,5+14]);
  }
  hull()
  {
    translate([0,0,-40]) cylinder (d=21+2*5, h=40+5);
    translate([0,0,-35]) rotate([0,0,90-73]) cube([20,16,35+5]);
  }
  rotate([0,0,-73]) hull()
  {
    translate([0,21/2+3,0]) rotate([0,90,0]) cylinder(d=M3insertD+4, h=13);
    translate([0,21/2+3,-32+5]) rotate([0,90,0]) cylinder(d=M3insertD+4, h=13);
  }
    
}
module frameCornerR(z3in1)
{
  hull()
  {
    translate([-20,-12-20,-21]) cube ([80,20,21]);
    translate([-20,-12-20-5,-21-5]) cube ([80,20,21]);
  }
  translate([xDiff2,-12-20,-20]) cube ([20,80,20]);
  translate([0,0,-60]) cylinder (d=LM12_D, h=80);
  translate([0,0,-40.2]) cylinder (d1=LM12_D+1, d2=LM12_D-0.1, h=1); //filet
  translate([0,0,5-0.5]) cylinder (d2=LM12_D+1, d1=LM12_D-0.1, h=1); // filet
 
  translate([xDiff2-5-M5head/2,-12+5,-10]) rotate([-90,0,0]) cylinder (d=M5head, h=16);
  translate([xDiff2-5-M5head/2,-12+5-10,-10]) rotate([-90,0,0]) cylinder (d=M5hole, h=50);

  translate([xDiff2-5,2,-10]) rotate([0,-90,0]) cylinder (d=M5head, h=xDiff2);
  translate([30,2,-10]) rotate([0,-90,0]) cylinder (d=M5hole, h=80);

  translate([0,-12-10,-10]) cylinder (d=M5hole, h=20);
  translate([xDiff2+10,10,-10]) cylinder (d=M5hole, h=20);
  
  rotate([0,0,-73])
  {
    translate([-9,0,-40]) cube([1,20,50]);
    translate([-9,0,-40]) cube([10,9,50]);
    translate([-9-19,19.5,-40]) cube([20,1,50]);
    translate([40,21/2+3,0]) rotate([0,-90,0]) cylinder(d=M3hole, h=60);
    translate([8,21/2+3,0]) rotate([0,90,0]) cylinder(d=M3insertD, h=60);
    translate([10+7,21/2+3,0]) rotate([0,90,0]) cylinder(d=M3insertD+2, h=60);

    translate([40,21/2+3,-32+5]) rotate([0,-90,0]) cylinder(d=M3hole, h=60);
    translate([5,21/2+3,-32+5]) rotate([0,90,0]) cylinder(d=M3insertD, h=60);
//      translate([10+7,21/2+3,-30+5]) rotate([0,90,0]) cylinder(d=M3insertD+2, h=60);

  }
  hull()
  {
    translate([xDiff2-5-1,15,-50]) cube([1,20,50-2]);
    rotate([0,0,-73]) translate([-9-19,19.5,-50]) cube([20,1,50-2]);
  }
}

module frameCorner()
{
  difference()
  {
    render() frameCornerA(z3in1 = false);
    render() frameCornerR(z3in1 = false);
  }
}

module zMiddle3in1(pitch = 80)
{
  difference()
  {
    render() union()
    {
      translate([0,-pitch,0]) render() frameCornerA(z3in1 = true);
      middleA(z3in1 = true);
      mirror([0,1,0]) translate([0,-pitch,0]) render() frameCornerA(z3in1 = true);
      translate([xDiff2-5,-pitch,-18]) cube([5+18,2*pitch,5+18]);
    }
    translate([0,-pitch,0]) render() frameCornerR(z3in1 = true);
    middleR(z3in1 = true);
    mirror([0,1,0]) translate([0,-pitch,0]) render() frameCornerR(z3in1 = true);
    translate([xDiff2,-pitch-1,-18]) cube([5+18,2*pitch+2,18]);
  }
}

color("purple",0.8) translate([(z3in1) ? 0 : 100,0,zLen+0.02]) render() top3in1();

color("purple",0.8) render() bottom();
*!color("purple",0.8) rotate([180,0,0]) render() bottom();

color("purple",0.8)  render() translate([(z3in1) ? 100 : 0,0,zLen+0.02]) top();
color("purple",0.8)  render() translate([(z3in1) ? 100 : 0,-80,zLen+0.02]) topRod();
*color("purple",0.8)  rotate([180,0,0]) render() topRod();
color("purple",0.8) mirror([0,0,1]) render() translate([0,-80,30.02]) topRod(-2);
color("purple",0.8) mirror([0,1,0]) render() translate([(z3in1) ? 100 : 0,-80,zLen+0.02]) topRod();
*color("purple",0.8)  mirror([0,1,0]) render() rotate([180,0,0]) topRod();
color("purple",0.8) mirror([0,1,0]) mirror([0,0,1]) render() translate([0,-80,30.02]) topRod(-2);


%color("gray",0.6) translate([0,-80,zPos-57+10-1]) cylinder(d=21, h=57);
%color("gray",0.6) mirror([0,1,0]) translate([0,-80,zPos-57+10-1]) cylinder(d=21, h=57);

color("lightgreen",0.8) translate([(z3in1) ? 0 : 100,0,zPos]) render() zMiddle3in1();
color("lightgreen",0.8) translate([(z3in1) ? 100 : 0,-80,zPos]) render() frameCorner();

color("lightgreen",0.8) translate([(z3in1) ? 100 : 0,0,0]) render() translate([0,0,zPos]) middle();
*!color("lightgreen",0.8) rotate([0,180,0]) render() translate([0,0,zPos]) middle();
color("lightgreen",0.8) mirror([0,1,0]) translate([(z3in1) ? 100 : 0,-80,zPos]) render() frameCorner();
*!color("lightgreen",0.8) rotate([0,180,0]) mirror([0,1,0]) render() frameCorner();

//tableCorner();
//mirror([0,1,0]) tableCorner();

%translate([0,0,zPos-0.01]) rotate([180,0,0]) color("orange", 0.4) cylinder(d=10.2, h=27.8);
%translate([0,0,zPos-0.01]) rotate([180,0,0]) color("orange", 0.4) cylinder(d=23, h=4);
%translate([0,0,-motorPlus]) color("gray", 0.4) render() nema17_T8(40,zLen+10);
//%translate([0,0,10-motorPlus]) color("gray", 0.4) cylinder(d=17.4*(1+sin(30)), h=10, $fn=6);
%translate([-30-xDiff-0.01,-280/2,-30-0.01]) color("silver", 0.4) cube([30,280,30]);
%translate([-30-xDiff-0.01,-280/2,zLen-30-0.01]) color("silver", 0.4) cube([30,280,30]);

%translate([xDiff2+0.01,-92,zPos-0.01]) color("silver", 0.4) rotate([0,90,0]) cube([20,92*2,20]);
%translate([xDiff2+0.01-47,-100/2-42-20-0.01,zPos-0.01]) color("silver", 0.4) rotate([0,90,0]) cube([20,20,150]);
%mirror([0,1,0]) translate([xDiff2+0.01-47,-100/2-42-20-0.01,zPos-0.01]) color("silver", 0.4) rotate([0,90,0]) cube([20,20,150]);

%translate([0,-80,-10-30]) color("silver", 0.4) cylinder(d=12, h=zLen+10+10+30+5+1);
%mirror([0,1,0]) translate([0,-80,-10-30]) color("silver", 0.4) cylinder(d=12, h=zLen+10+10+30+5+1);




