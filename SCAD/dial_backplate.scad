// Author: Dieter Vanderfaeillie
// Dail gauge backplate
// Licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
$fn = 70;

RADIUS = 26;       //Backplate radius
SCREWRADIUS = 1; //Small screws radius

difference(){
  union() {
    cylinder(r = RADIUS,h=1);
    translate([0,0,1]) cylinder(r1 = RADIUS, r2= RADIUS-0.5 ,h=1);
    translate([0,0,2]) cylinder(d1 = 18, d2 = 17.5, h=1);
  }
    screwholes();   
}
connector();

module screwholes(){
    rotate(a=-45, v=[0,0,1]){
        screw();
    }
    rotate(a=-45-90, v=[0,0,1]){
        screw();
    }
    rotate(a=45+90, v=[0,0,1]){
        screw();
    }
    rotate(a=45, v=[0,0,1]){
        screw();
    }
}

module screw(){
    translate([45/2,0,0]){
        translate([0,0,-1]) cylinder(r=SCREWRADIUS,h=2+1);
        translate([0,0,1]){
            cylinder(r=SCREWRADIUS*1.9,h=2);
        }
}
}

module connector(){
    translate([2.5,-8,0]){
        rotate(a=-90,v=[0,1,0]){
            difference(){
                union(){
                    translate([14,8,0]){
                        cylinder(r=8,h=5);
                    }
                    cube([14,16,5]);
                }
                translate([14,8,-1]){
                    cylinder(r=2.9,h=7);
                }
            }
        }
    }
}
