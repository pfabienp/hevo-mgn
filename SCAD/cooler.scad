

module cooler_404011()
{
  translate([-40/2,-40/2,0]) difference()
  {
     cube([40,40,11]);
    for(i=[0:8])
    {
      translate([(i+0.5)*4.2,-1,1.5]) cube([2,42,11]);
    }
    for(i=[0:11])
    {
      translate([-1,i*3.2+(3.2-2),1.5]) cube([42,2,11]);
    }
  }
}

module cooler_404011_hole()
{
  translate([-50/2,-42/2,0]) 
    cube([50,42,11]);
}

cooler_404011();

    
